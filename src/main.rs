#![no_std]
#![no_main]

use panic_halt as _;
use attiny_hal::{prelude::_embedded_hal_blocking_delay_DelayMs, port::{Pin, mode::Output, PinOps}};

#[attiny_hal::entry]
fn main() -> ! {
    let dp = attiny_hal::Peripherals::take().unwrap();
    let pins = attiny_hal::pins!(dp);

    // Set up LED pinds
    let mut error_led = pins.pb0.into_output();
    let mut morse_led = pins.pb1.into_output();

    // Create the timer appropriate to our clock speed
    let mut delay = attiny_hal::delay::Delay::<attiny_hal::clock::MHz1>::new();

    loop {
        // Pulse the error led once to indicate start of message
        pulse(&mut error_led, &mut delay, INTER_CHAR_DELAY);

        pulse_message(&MESSAGE_PULSES, &mut morse_led, &mut delay);
        
        // Wait for the next round
        delay.delay_ms(INTER_MESSAGE_DELAY);
    }
}

type PulsePattern = u8;

// Given an array of pre-computed patterns (see https://gitlab.com/dchenbecker/morse_lut_generator), iterate over the array
// and emit the pulses
fn pulse_message<P: PinOps, D>(message: &[PulsePattern], led: &mut Pin<Output, P>, delay: &mut D) 
where D: _embedded_hal_blocking_delay_DelayMs<u16>
{
    let length = message.len();
    
    for (index, pattern) in message.iter().enumerate() {
        // whitespace just turns into pauses
        if *pattern == WHITESPACE_PATTERN {
            delay.delay_ms(INTER_CHAR_DELAY);
            continue;
        }

        pulse_pattern(*pattern, led, delay);

        // If we haven't reached the end of the string, pause between characters
        if (index + 1) < length {
            delay.delay_ms(INTER_CHAR_DELAY);
        }
    }
}

fn pulse_pattern<P, D>(pattern: PulsePattern, led: &mut Pin<Output, P>, delay: &mut D)
where D :  _embedded_hal_blocking_delay_DelayMs<u16>, P : PinOps {
    let mut pulses = pattern;

    // Extract the length from the least sig bits
    let sequence_size: u8 = pulses & LEN_MASK;

    // Shift off the length
    pulses = pulses >> 3;

    for index in 1..=sequence_size {
        match (pulses & PULSE_MASK) != 0 {
            DOT => pulse(led, delay, DOT_DURATION),
            DASH => pulse(led, delay, DASH_DURATION),
        }

        // shift to prepare for the next symbol
        pulses = pulses >> 1;

        // We only have an inter-pulse delay if we're not on the last pulse
        if index < sequence_size {
            delay.delay_ms(INTER_PULSE_DELAY);
        }
    }
}

fn pulse<P, D>(led: &mut Pin<Output, P>, delay: &mut D, duration: u16)
where D :  _embedded_hal_blocking_delay_DelayMs<u16>, P : PinOps {
    led.set_high();
    delay.delay_ms(duration);
    led.set_low();
}

const DOT: bool = false;
const DASH: bool = true;
// A null byte (zero length pattern) means a whitespace pause
const WHITESPACE_PATTERN: u8 = 0;
const PULSE_MASK: u8 = 0x01;
const LEN_MASK: u8 = 0x07;

const DOT_DURATION: u16 = 150;
const DASH_DURATION: u16 = 400;

const INTER_PULSE_DELAY: u16 = 100;
const INTER_CHAR_DELAY: u16 = 500;
const INTER_MESSAGE_DELAY: u16 = 1200;

const MESSAGE_PULSES: [u8;19] = [
  0x04, // 'h'
  0x02, // 'i'
  0x00, // <whitespace>
  0x1a, // 'm'
  0x6c, // 'y'
  0x00, // <whitespace>
  0x0a, // 'n'
  0x12, // 'a'
  0x1a, // 'm'
  0x01, // 'e'
  0x00, // <whitespace>
  0x02, // 'i'
  0x03, // 's'
  0x00, // <whitespace>
  0x0b, // 'd'
  0x01, // 'e'
  0x13, // 'r'
  0x01, // 'e'
  0x2b, // 'k'
];

#!/usr/bin/env zsh

print "Writing $1"

# I'm using WSL, so this needs to run under sudo
sudo avrdude -v -pattiny85 -c usbtiny -U "flash:w:${1}:e"
